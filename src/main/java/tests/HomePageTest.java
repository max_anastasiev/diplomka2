package tests;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import core.BaseTest;
import core.GINDriver;
import poms.HomePage;
import poms.SubNewsFreelancerPage;

    @Test
    public class HomePageTest extends BaseTest {

        private static final Logger log = LogManager.getLogger(HomePageTest.class);

        public void test(){

            GINDriver gin = GINDriver.init(driver);
            gin.hp.submitnews();

        }

}
