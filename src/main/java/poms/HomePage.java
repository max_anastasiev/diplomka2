package poms;

import controls.WebButton;
import core.AbstractPOM;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;


public class HomePage  extends AbstractPOM {


    @FindBy(xpath = "//*[@id=\"terms\"]")
    public WebButton termsChBtn;

    @FindBy(xpath = "//*[@id=\"block-block-1\"]/p[2]/a")
    public WebButton submitnewsBtn;

    @FindBy(xpath = "//*[@id=\"block-block-1\"]/div/div[2]/a")
    public WebButton loginBtn;

    @FindBy(xpath = "//*[@id=\"block-block-1\"]/div/div[1]/a")
    public   WebButton contactBtn;


    public HomePage (WebDriver driver){ super(driver); }

    public void submitnews() {
        termsChBtn.click();
        submitnewsBtn.click();
    }


}
