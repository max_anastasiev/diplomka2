package core;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public abstract class BaseTest {

    private static final Logger log = LogManager.getLogger(BaseTest.class);
    protected static WebDriver driver;
    private String url = "http://gin.cs1.soft-tehnica.com/";

    @BeforeClass
    public void setup(){
        log.info(getClass().getSimpleName() + " test is in progress");
        log.info("Open browser");
        driver = new ChromeDriver();
//		driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        log.info("Navigate to " + url);
        driver.get(url);
    }

    @AfterClass
    public void close(){
//        takeScreenshot();
        log.info("Haha, game over");
        driver.quit();
    }

    public static void takeScreenshot(){
        TakesScreenshot scrShot =((TakesScreenshot)driver);
        File srcFile=scrShot.getScreenshotAs(OutputType.FILE);
        LocalDateTime timePoint = LocalDateTime.now();
        String name = "screens/screenshot "  + timePoint.get(ChronoField.YEAR_OF_ERA) + "-" + timePoint.getMonth() + "-" + timePoint.getDayOfMonth() + "|"+ timePoint.getHour() + "-" + timePoint.getMinute() + "-" + timePoint.getSecond() + ".png";
        File destFile=new File(name);
        log.info("Screenshot name is " + name);
        try {
            FileUtils.copyFile(srcFile, destFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
