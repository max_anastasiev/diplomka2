package core;

import org.openqa.selenium.WebDriver;

import poms.HomePage;
import poms.SubNewsFreelancerPage;

public class GINDriver {

        private static GINDriver ginDriver;

        public HomePage hp;
        public SubNewsFreelancerPage snf;

        private GINDriver(WebDriver driver) {
            hp = new HomePage(driver);
            snf = new SubNewsFreelancerPage(driver);
        }

        public static GINDriver init(WebDriver driver) {
            if(ginDriver==null) {
                ginDriver = new GINDriver(driver);
            }
            return ginDriver;
        }

}
