package core;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import controls.WebTypifiedElement;


public class Helpers {

    private static final Logger log = LogManager.getLogger(Helpers.class);

    public static void click(WebDriver driver, WebElement a, WebElement b) {
        try {
            String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover',true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
            ((JavascriptExecutor) driver).executeScript(mouseOverScript, a);
            Thread.sleep(1000);
            ((JavascriptExecutor) driver).executeScript(mouseOverScript, b);
            Thread.sleep(1000);
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", b);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public static void check2StringIfEquals(WebTypifiedElement actual, String expected, String msg) {
        log.debug("Check if element is displayed");
        Assert.assertTrue(actual.isDisplayed(), "Element is displayed");
        check2StringIfEquals(actual.getText(), expected, msg);
    }

    public static void check2StringIfEquals(String actual, String expected, String msg) {
        log.debug("Check 2 strings if equals\nActual\t:" + actual + "\nExpected:" + expected);
        Assert.assertTrue(StringUtils.equals(actual, expected), msg);
    }
}
